package adeo.leroymerlin.cdp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EventService {

    private final EventRepository eventRepository;

    @Autowired
    public EventService(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public List<Event> getEvents() {
        return eventRepository.findAllBy();
    }

    public void delete(Long id) {
        eventRepository.delete(id);
    }

 
    public List<Event> getFilteredEvents(String query) {
        List<Event> events = eventRepository.findAllBy();
        // Filter the events list in pure JAVA here
        
        List<Event> filterEvents = events.stream()
       /* [PHS] Filter avec event et band
        N.B: Pink Floyd exists in 2 events 
        .filter(i -> i.getTitle().contains("ing")) 
          [PHS] Filter event or band containg matchinhg query
        .filter(i -> i.getBands().stream().anyMatch(s -> s.getName().contains("ing")) || i.getTitle().contains("ing"))  
        .filter(i -> i.getBands().stream().anyMatch(s -> s.getName().contains(query))) */
        		
        		.filter(i -> i.getBands().stream()
        				.anyMatch(s -> s.getMembers().stream()
        				.anyMatch(m -> m.getName().contains(query))))
        
        .collect(Collectors.toList());        
        
        return filterEvents;
    }
    
    /* PHS Create update method to wire the bean */
    public void save(Event event) {
        eventRepository.setFixedCommentNbStarsFor(event.getComment(), event.getNbStars(), event.getId());
    }    
}
