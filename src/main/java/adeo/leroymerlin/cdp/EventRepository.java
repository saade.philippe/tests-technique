package adeo.leroymerlin.cdp;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/* PHS make the transaction writable in order to delete or update */
@Transactional(readOnly = false, rollbackFor = {Exception.class})
public interface EventRepository extends Repository<Event, Long> {
	

    void delete(Long eventId);
    
    List<Event> findAllBy();

    /* [PHS] update method */

    /* save only event without band delete queries and insert 1 query*/
    // Event save(Event event);

    @Modifying
    @Query("update Event u set u.comment = ?1, u.nbStars = ?2 where u.id = ?3")
    int setFixedCommentNbStarsFor(String comment, Integer nbStars, Long id);

}
